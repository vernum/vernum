
WIP
===

Notes on work in progress

For issues:

- Handle bootstrap case (first versioned release)
- Full component syntax
- Confirm the no-blob clone works as expected
- Enable manual default increment releases on default branch if no variable set
- Alternate branch strategies, such as "next" branches for alphas
- Should the default behaviour be to tag in Git first?
- template replacement example for Poetry
- Input to leave off the "v"
- Support transitions from one scheme to another
