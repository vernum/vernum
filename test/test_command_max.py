from tempfile import NamedTemporaryFile
from unittest import TestCase
from unittest.mock import Mock
from unittest.mock import patch
from io import StringIO

from wizlib.input_handler import InputHandler
from test import testfile

from vernum.command.max_command import MaxCommand
from vernum.command import VerNumCommand
from vernum import VerNumApp


class TestCommandMax(TestCase):

    def test_max(self):
        tx = "v1.2.3\nv1.0.6"
        c = MaxCommand(input=InputHandler.fake(tx))
        n = c.execute()
        self.assertEqual(n, '1.2.3')

    def test_from_app(self):
        with patch('sys.stdout', o := StringIO()):
            with patch('sys.stderr', e := StringIO()):
                with testfile("v1.2.3\nv1.0.6") as file:
                    VerNumApp.run('-i', file.name, 'max', debug=True)
        o.seek(0)
        self.assertEqual(o.read(), '1.2.3')
        e.seek(0)
        self.assertTrue(e.read().startswith('Found'))

    def test_from_stdin(self):
        with patch('sys.stdout', o := StringIO()):
            with patch('sys.stdin', StringIO("v1.2.3\n1.3.0\nv1.2.9")):
                VerNumApp.run('max', debug=True)
        o.seek(0)
        self.assertEqual(o.read(), '1.3.0')

    def test_no_input(self):
        c = MaxCommand()
        n = c.execute()
        self.assertIsNone(n)
