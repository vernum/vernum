# Assume venv; for dev only

.PHONY: init test style all build

PROJECT_NAME := $(shell basename "$(CURDIR)")

all: style test

# Set up a new venv
init:
	rm -rf .venv
	python3.11 -m venv .venv
	.venv/bin/pip install --upgrade pip poetry

# Test and show coverage
test:
	.venv/bin/python -m coverage run --source=$(PROJECT_NAME) -m unittest -v
	.venv/bin/python -m coverage report -m --fail-under 96

# Clean up and show style
style:
	.venv/bin/autopep8 -ir .
	.venv/bin/pycodestyle $(PROJECT_NAME)
	.venv/bin/pycodestyle test

build:
	rm -rf dist
	.venv/bin/poetry build